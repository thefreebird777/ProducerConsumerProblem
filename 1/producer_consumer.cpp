#include <iostream>
#include <fstream>
#include <iterator> 
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define RANDOM_MAX 10 

using namespace std;

vector<string> buffer;
pthread_mutex_t mutex= PTHREAD_MUTEX_INITIALIZER;
sem_t empty;
sem_t full;

void *producer(void* v){
	int i = 0;
	vector<string> *v_ptr = (vector<string>*) v;
	while(true){
		sem_wait(&empty);
		pthread_mutex_lock(&mutex);
		sleep(rand() % RANDOM_MAX);
		i = rand() % v_ptr->size();
		cout <<"[producer thread ID:" << pthread_self() <<"] inserted an item (word:"<< v_ptr->at(i) <<") from the buffer" << endl;
		buffer.push_back(v_ptr->at(i));
		pthread_mutex_unlock(&mutex);
		sem_post(&full);
	}
	pthread_exit(0);
}

void *consumer(void* v){
	int i = 0;
	while(true){
		sem_wait(&full);
		pthread_mutex_lock(&mutex);
		sleep(rand() % RANDOM_MAX);
		i = rand() % buffer.size();
		cout <<"[consumer thread ID:" << pthread_self() <<"] removed an item (word:"<< buffer.at(i) <<") from the buffer" << endl;
		buffer.erase(buffer.begin() + i);
		pthread_mutex_unlock(&mutex);
		sem_post(&empty);
	}
	pthread_exit(0);
}

int main(int argc, char *argv[]){
	int sleepTime = atoi(argv[1]);
	int numProducers = atoi(argv[2]);
	int numConsumers = atoi(argv[3]);
	string in;
	vector<string> vec;
	pthread_t *ptid;
	ptid = (pthread_t*)malloc(sizeof(pthread_t)*numProducers);
	pthread_t *ctid;
	ctid = (pthread_t*)malloc(sizeof(pthread_t)*numConsumers);
	ifstream input("wordsEn.txt");
	if (input.is_open()){
		while (input >> in) {
			vec.push_back(in);
		}
	}else{
		cout<< "File \"wordsEn.txt\"  Was Not Found." <<endl;
		exit(1);
	}

	sem_init(&empty,0, numProducers);
	sem_init(&full, 0, 0);

	for(int i = 0; i < numProducers; i++){
		pthread_create(&ptid[i], NULL, producer, &vec);
	}	
	for(int i = 0; i < numConsumers; i++){
		pthread_create(&ctid[i], NULL, consumer, NULL);
	}
	sleep(sleepTime);
	return 0;
}
